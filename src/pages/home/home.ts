import {Component} from '@angular/core';
import {
  BackgroundGeolocation,
  BackgroundGeolocationConfig,
  BackgroundGeolocationResponse
} from '@ionic-native/background-geolocation';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  location: {long: number, lat: number}[] = [];

  constructor(private backgroundGeolocation: BackgroundGeolocation) {
    const config: BackgroundGeolocationConfig = {
      desiredAccuracy: 10,
      stationaryRadius: 20,
      interval: 5000,
      stopOnTerminate: true,
      distanceFilter: 30,
    };

    this.backgroundGeolocation.configure(config)
      .subscribe((location: BackgroundGeolocationResponse) => {

        this.location.push({long: location.longitude, lat: location.latitude});
        this.backgroundGeolocation.finish(); // FOR IOS ONLY
      });

    this.backgroundGeolocation.start();
  }

  getCoords() {
// If you wish to turn OFF background-tracking, call the #stop method.
    this.backgroundGeolocation.stop();
  }
}
